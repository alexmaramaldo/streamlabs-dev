# Streamlabs Dev!

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

# Introduction:
Hi, My Name is Alex Maramaldo, and email is: alexmaramaldo@gmail.com

This project was made to meet the specifications of the link: https://gist.github.com/osamakhn/14a378f3107d49de47e0b617a3d5fdf5
*DEMO*: https://streamlabs-alex-maramaldo-dev.herokuapp.com

All technologies used here, is only to show the possible that i can work, I did not put it a stronger layout and others beautiful items, this is functional, hugs and i hope see you a coming soon!

# Introduction:
In My project, i used Laravel and VueJS, but you can check my Customs PHP Classes inside folder: `/app/TwitchApi`
The project is divides into parts, they are:
1. Screen to Login, when click in the button, you are redirected to Login with Twich, and callback to Second Screen.
2. Screen to Search. here, you can search a Streamer, i used the API from Twitch for this, and return 10 results, and you can click in the item and go to Third Screen.
3. This screen is loaded Live Video from Streamer, and Chat, this moment, the backend receive a request and after this, start LISTEN all events(only following this moment) from Streamer, here, we can use more others examples, this is possible, and, using broacast, the interface is updated when happen a new event

The project dont use Database, only api from `https://pusher.com/` to notifications, here, we can change this to use *REDIS* per example!

# QUESTIONS
1. How would you deploy the above on AWS? (ideally a rough architecture diagram will help)
Answer: Would create the enviroment, using Elastic Load Balance, we can use auto scalling to put up more instances or removes instances, this is very important, because we dont need a lot process if we have less trafic, but, if we need, we can create dynamically, new horizontal instances.

![Amazon](http://awsmedia.s3.amazonaws.com/2012-02-24-techdoc-elb-arch.png)

2. Where do you see bottlenecks in your proposed architecture and how would you approach scaling this app starting from 100 reqs/day to 900MM reqs/day over 6 months?
Answer: The mainly bottlenecks to *INFRASTRUCTURE*, due to use auto scalling, we can not use local resources, per example, local cache, local database, and, if we need create a redis, per example, we need a specifical structure, but, if this new structure dont have a best setup, the current INFRASTRUCTURE will have a bottleneck, and, bottlenecks inside project is the *Pusher API*, because i used a free version today(i spoke in the presentation that we can easily switch to using a Redis), and the number from request inside Twitch API, today is only 800 reqs/minute, this is a problem, so, we need create a cache routine to get a content, per example, SEARCH, to each search we can cache results inside project, and, after this, we can reduce numbers from requests to Twitch API, about server i can't see problem, because have auto scalling, and, with this, we will have UP 100% from time.

