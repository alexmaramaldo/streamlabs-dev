<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/stream/{streamer}/{streamerId}', "StreamsAPIController@stream");
Route::get('/stream/{streamer}/{streamerId}/events', "StreamsAPIController@events");
Route::get('/stream/{streamer}/{streamerId}/summary', "StreamsAPIController@streamSummaries");
Route::get('/stream/{streamer}/{streamerId}/register', "StreamsAPIController@registerWatching");


Route::get('/search/{query}', "SearchAPIController@search");
