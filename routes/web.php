<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('/auth', "AuthController@redirectToTwitchAuthentication");
Route::get('/twitch/callback', "AuthController@handleTwitchCallback");

Route::post('/twitch/webhook', "WebHookController@webhook");
Route::get('/twitch/webhook', "WebHookController@webhook");

Route::get('/{any}', 'SinglePageController@index')->where('any', '.*');
