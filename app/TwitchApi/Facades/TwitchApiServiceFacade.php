<?php

namespace TwitchApi\Facades;

use Illuminate\Support\Facades\Facade;

class TwitchApiServiceFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'TwitchApi\Services\TwitchApiService';
    }
}
