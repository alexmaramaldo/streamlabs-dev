<?php

namespace TwitchApi\API;

/**
 * Class Channels
 * @package TwitchApi\API
 */
class Channels extends API
{
    /**
     * Get channel object.
     *
     * @param string $channel Channel name
     *
     * @return JSON Channel object
     */
    public function channel($channel)
    {
        return $this->sendRequest('GET', 'channels/'.$channel);
    }

    /**
     * Get Event.
     *
     * @param string $channel Channel name
     * @param string $token   Twitch token
     *
     * @return JSON Request result with new stream key
     */
    public function events($channel, $token = null)
    {
        return $this->sendRequest('GET', 'channels/'.$channel.'/events?past_events=true');
    }
}
