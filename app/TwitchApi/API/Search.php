<?php

namespace TwitchApi\API;

/**
 * Class Search
 * @package TwitchApi\API
 */
class Search extends Api
{
    /**
     * Search channels.
     *
     * @param string $options Search options
     *
     * @return JSON Search results
     */
    public function searchChannels($options)
    {
        $availableOptions = ['query', 'limit', 'offset'];

        return $this->sendRequest('GET', 'search/channels', false, $options, $availableOptions);
    }
}
