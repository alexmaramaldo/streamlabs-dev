<?php
namespace TwitchApi\API;

class Users extends Api
{
    public function authenticatedUser($token = null)
    {
        return $this->sendRequest('GET', 'user', $this->getToken($token));
    }
}