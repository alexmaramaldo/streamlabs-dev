<?php

namespace TwitchApi\Services;

use TwitchApi\API\Api;
use TwitchApi\API\Authentication;
use TwitchApi\API\Channels;
use TwitchApi\API\Search;
use TwitchApi\API\Streams;
use TwitchApi\API\Users;

class TwitchApiService extends Api
{
    /**
     * Authentication.
     */
    public function getAuthenticationUrl($state = null, $forceVerify = false)
    {
        $authentication = new Authentication();

        return $authentication->getAuthenticationUrl($state, $forceVerify);
    }

    public function getAccessToken($code, $state = null)
    {
        $authentication = new Authentication();

        return $authentication->getAccessToken($code, $state);
    }

    public function getAccessObject($code, $state = null)
    {
        $authentication = new Authentication();

        return $authentication->getAccessObject($code, $state);
    }

    /**
     * Channels.
     */
    public function channel($channel)
    {
        $channels = new Channels();

        return $channels->channel($channel);
    }

    public function events($channel)
    {
        $channels = new Channels();

        return $channels->events($channel);
    }


    /**
     * Search.
     */
    public function searchChannels($options)
    {
        $search = new Search();

        return $search->searchChannels($options);
    }

    /**
     * Streams.
     */
    public function liveChannel($channel)
    {
        $streams = new Streams();

        return $streams->streamsChannel($channel);
    }

    public function streams($options)
    {
        $streams = new Streams();

        return $streams->streams($options);
    }

    public function authUser($token = null)
    {
        $users = new Users();
        return $users->authenticatedUser($this->getToken($token));
    }

    public function streamSummaries($options = [])
    {
        $streams = new Streams();
        return $streams->streamSummaries($options);
    }

    public function featuredStreams($options = [])
    {
        $streams = new Streams();
        return $streams->streamsFeatured($options);
    }

    /**
     * @param $streamerId
     * @return \TwitchApi\API\JSON
     */
    public function registerWebHook($streamerId, $token)
    {
        $streams = new Streams();
        return $streams->registerWebHook($streamerId, $token);
    }
}
