<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use TwitchApi;

class StreamsAPIController extends Controller
{
    public function stream($streamer, $streamerId)
    {
        return TwitchApi::liveChannel($streamerId);
    }

    public function events($streamer, $streamerId)
    {
        return TwitchApi::events($streamerId);
    }

    public function streamSummaries($streamer, $streamerId)
    {
        return TwitchApi::streamSummaries();
    }

    public function registerWatching($streamer, $streamerId, Request $request)
    {
        TwitchApi::setToken($request->get("token"));
        return TwitchApi::registerWebHook($streamerId, html_entity_decode($request->get("token")));
    }
}
