<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use TwitchApi;
use Illuminate\Http\Request;

class SearchAPIController extends Controller
{
    public function search($query, Request $request)
    {
        $options = [
            'query'  => $query,
        ];

        return TwitchApi::searchChannels($options);
    }
}
