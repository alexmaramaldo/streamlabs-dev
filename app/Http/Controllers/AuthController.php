<?php

namespace App\Http\Controllers;

use function html_entity_decode;
use Illuminate\Http\Request;
use function redirect;
use TwitchApi;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    public function redirectToTwitchAuthentication()
    {
        return redirect(TwitchApi::getAuthenticationUrl());
    }

    public function handleTwitchCallback(Request $request)
    {

        // Request Twitch token from Twitch
        $code = $request->input('code');
        $response = TwitchApi::getAccessObject($code);
        TwitchApi::setToken($response['access_token']);

        // Get user object from Twitch
        $twitchUser = TwitchApi::authUser();

        $data = ["user" => $twitchUser['name']];
        Mail::send('templates.emails', compact('data'), function($message) {
            $message->to("alex@osbox.com.br", "alex@osbox.com.br");
            $message->subject("New Event.");
        });

        return redirect("/search")
            ->cookie("userDataName", $twitchUser['name'], 43200, "/", "streamlabs-alex-maramaldo-dev.herokuapp.com", 1, false)
            ->cookie("userDataLogo", $twitchUser['logo'], 43200, "/", "streamlabs-alex-maramaldo-dev.herokuapp.com", 1, false)
            ->cookie("userTwitchToken", html_entity_decode($response['access_token']), 43200, "/", "streamlabs-alex-maramaldo-dev.herokuapp.com", 1, false);
    }
}
