<?php

namespace App\Http\Controllers;

use App\Events\StreamerEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class WebHookController extends Controller
{
    public function webhook(Request $request)
    {

        $data =  $request->all();

        if($request->has("hub_challenge")) {
            return response($request->get("hub_challenge"), 200);
        } else {
            event(new StreamerEvent($data));

            return response(null, 200);
        }
    }
}
