<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>StreamLabs Alex Maramaldo App</title>
    <link href="/css/app.css" rel="stylesheet">
</head>
<body>
<div id="app">
    <app></app>
</div>
<!-- Load the Twitch embed script -->
<script type="text/javascript" src="https://embed.twitch.tv/embed/v1.js"></script>
<script type="text/javascript" src="/js/app-internal.js"></script>
</body>
</html>