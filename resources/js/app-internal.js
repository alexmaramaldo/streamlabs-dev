
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

import Vue from 'vue'
import VueRouter from 'vue-router'
import LoadScript from 'vue-plugin-load-script'
import VueCookies from 'vue-cookies'

import Echo from "laravel-echo"
window.Pusher = require('pusher-js')

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: 'a3ea771ae6b51341e8f2',
    cluster: 'us2',
    encrypted: true
})


Vue.use(VueCookies)
Vue.use(LoadScript)
Vue.use(VueRouter)

import App from './views/AppView'
import Search from './views/SearchView'
import Streamer from './views/StreamerView'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/home',
            name: 'home',
            component: Search
        },
        {
            path: '/search',
            name: 'search',
            component: Search
        },
        {
            path: '/streamer/:streamer/:streamerId',
            name: 'streamer',
            component: Streamer
        },
    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});
